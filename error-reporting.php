<?php
/*
Plugin Name:  Error Reporting
Description:  Hides deprecated errors
Version:      1.0.0
License:      MIT License
*/

if (WP_DEBUG) {
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
}